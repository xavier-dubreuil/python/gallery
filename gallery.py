from app import render_template
from json import loads, dumps
from file import get
from os import environ
from os.path import join

def _get_galleries(name):
    try:
        content = get(join(environ['CONFIG_PATH'], name + '.json'))
        config = loads(content.decode('utf-8'))
        return config
    except:
        pass
    return None


def index():
    config = _get_galleries('index')
    if config is not None:
        return render_template('index.html', config=config)
    return render_template('not_found.html')


def gallery(slug):
    config = _get_galleries('index')
    gallery = _get_galleries(slug)
    if config is not None or gallery is not None:
        return render_template('gallery2.html', config=config, gallery=gallery)
    return render_template('not_found.html')
