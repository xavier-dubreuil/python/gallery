from PIL import Image
from file import get, key, put
from io import BytesIO
from os import environ
from os.path import splitext, join


class Resize:

    def __init__(self, dsn):
        self.dsn = dsn
        content = get(dsn)
        self.image = Image.open(BytesIO(content))

    def cache_key(self, function, width, height):
        file_name, extension = splitext(self.dsn)
        return file_name + '.' + function + '.' + width + 'x' + height + extension

    def get_cache(self, function, width, height):
        if 'CACHE_PATH' not in environ:
            return None
        cache_key = join(environ['CACHE_PATH'], key(self.cache_key(function, width, height)))
        cache = get(cache_key)
        if cache is not None:
            return Image.open(BytesIO(cache))

    def set_cache(self, function, width, height, image):
        if 'CACHE_PATH' not in environ:
            return None
        cache_key = join(environ['CACHE_PATH'], key(self.cache_key(function, width, height)))
        buffer = BytesIO()
        image.convert('RGB').save(buffer, 'webp')
        put(cache_key, buffer.getvalue())

    def fit_in(self, width, height):
        width_ratio = self.image.width / width
        height_ratio = self.image.height / height

        if height_ratio > width_ratio:
            return self.image.resize((int(self.image.width * height / self.image.height), height), Image.ANTIALIAS)
        return self.image.resize((width, int(self.image.height * width / self.image.width)), Image.ANTIALIAS)

    def fill_in(self, width, height):
        width_ratio = self.image.width / width
        height_ratio = self.image.height / height

        if height_ratio > width_ratio:
            resize_width = width
            resize_height = int(self.image.height * width / self.image.width)
        else:
            resize_width = int(self.image.width * height / self.image.height)
            resize_height = height

        new = self.image.resize((resize_width, resize_height), Image.ANTIALIAS)
        left = (resize_width - width) / 2
        upper = (resize_height - height) / 2
        return new.crop((left, upper, left + width, upper + height))

    def resize(self, function, width, height):
        cache = self.get_cache(function, width, height)
        if cache is not None:
            return cache
        if function == "fit-in":
            res = self.fit_in(int(width), int(height))
        elif function == "fill-in":
            res = self.fill_in(int(width), int(height))
        else:
            return self.image
        self.set_cache(function, width, height, res)
        return res


if __name__ == '__main__':
    test = Resize('file://images/usa-123_gD8jRqK7JJXjUOb9Z5tdK7LPSy9pBH63.jpg')
    plop = test.resize('fit-in', 200, 200)
    print(plop)
