from re import match
from boto3 import resource
from requests import get as rget
from os import makedirs
from os.path import dirname


def http_get(match):
    try:
        response = rget(match[0])
        return response.content
    except:
        return Nona


def http_key(match):
    return 'web/' + match[0].replace('http://', '').replace('http://', '')


def file_get(match):
    try:
        fd = open(match[0], 'rb')
        return fd.read()
    except:
        return None


def file_key(match):
    return 'file/' + match[0].replace('file://', '')


def file_put(match, content):
    try:
        makedirs(dirname(match[0]), exist_ok=True)
        fd = open(match[0], 'wb')
        fd.write(content)
    except:
        pass


def s3_obj(match):
    if len(match) == 4:
        s3 = resource('s3', aws_access_key_id=match[0], aws_secret_access_key=match[1])
        return s3.Object(bucket_name=match[2], key=match[3])
    s3 = resource('s3')
    return s3.Object(bucket_name=match[0], key=match[1])


def s3_get(match):
    try:
        object = s3_obj(match)
        obj_body = object.get()['Body'].read()
        return obj_body
    except:
        return None


def s3_key(match):
    if len(match) == 4:
        return 's3/' + match[2] + '/' + match[3]
    return 's3/' + match[0] + '/' + match[1]


def s3_put(match, buffer):
    try:
        object = s3_obj(match)
        object.put(Body=buffer, ContentType='image/webp')
    except:
        pass


regexps = {
    '^file://(.*)$': {
        'get': file_get,
        'key': file_key,
        'put': file_put,
    },
    '^(https?://.*)$': {
        'get': http_get,
        'key': http_key,
    },
    '^s3://([^/]*)/(.*)$': {
        'get': s3_get,
        'key': s3_key,
        'put': s3_put,
    },
    '^s3://([^:]*):([^@]*)@([^/]*)/(.*)$': {
        'get': s3_get,
        'key': s3_key,
        'put': s3_put,
    },
}


def _call_func(dsn, func):
    try:
        for regexp, funcs in regexps.items():
            res = match(regexp, dsn)
            if res is not None and func in funcs:
                return funcs[func], res.groups()
    except:
        pass
    return None, None


def get(dsn):
    func, match = _call_func(dsn, 'get')
    if func is not None:
        return func(match)
    return None


def put(dsn, buffer):
    func, match = _call_func(dsn, 'put')
    if func is not None:
        return func(match, buffer)


def key(dsn):
    func, match = _call_func(dsn, 'key')
    if func is not None:
        return func(match)
    return None
