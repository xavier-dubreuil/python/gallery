from app import app, render_template
from flask import Flask, make_response
from os import environ
from gallery import index, gallery
from io import BytesIO
from image import Resize
from base64 import b64encode, b64decode
from os.path import join
from re import match
from PIL import Image

debug = True if 'DEBUG' in environ and environ['DEBUG'] else False

class Response:

    def __init__(self, status_code, body):
        self.status_code = status_code
        self.body = body

    def flask_format(self):
        if isinstance(self.body, Image.Image):
            buffer = BytesIO()
            self.body.convert('RGB').save(buffer, 'webp')
            response = make_response(buffer.getvalue())
            response.headers['Content-Type'] = 'image/webp'
            return response
        return self.body

    def lambda_format(self):
        if isinstance(self.body, Image.Image):
            buffer = BytesIO()
            self.body.convert('RGB').save(buffer, 'webp')
            return {
                'statusCode': 200,
                'body': b64encode(buffer.getvalue()).decode(),
                'isBase64Encoded': True,
            }
        return {
            'statusCode': self.status_code,
            'body': self.body,
            'headers': {
                'Content-Type': 'text/html'
            }
        }


@app.template_filter('media')
def media(s, func, width, height):
    return '/' + join('media', func, str(width) + 'x' + str(height), b64encode(s.encode()).decode())


def gallery_index(match):
    return Response(200, index())


def gallery_gallery(match):
    return Response(200, gallery(match[0]))


def gallery_media(match):
    path = b64decode(match[3]).decode()
    resize = Resize(path)
    new = resize.resize(match[0], match[1], match[2])
    return Response(200, new)


urls = {
    '^/$': gallery_index,
    '^/media/([^/]*)/([^x]*)x([^/]*)/(.*)$': gallery_media,
    '^/([^/]*)$': gallery_gallery,
}


def handle(url):
    for regexp, func in urls.items():
        res = match(regexp, url)
        if res is not None:
            return func(res.groups())
    return Response(404, 'URL not found: '+url)


@app.route('/', methods=['GET'])
@app.route('/<path:text>', methods=['GET'])
def all_routes(text=''):
    return handle('/' + text).flask_format()


def lambda_handler(event, context):
    return handle(event['path']).lambda_format()


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=debug)
