from flask import Flask

app = Flask('gallery')

def render_template(template, **kwargs):
    tpl = app.jinja_env.get_template(template)
    return tpl.render(kwargs)
